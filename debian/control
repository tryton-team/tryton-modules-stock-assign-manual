Source: tryton-modules-stock-assign-manual
Section: python
Priority: optional
Maintainer: Debian Tryton Maintainers <team+tryton-team@tracker.debian.org>
Uploaders: Mathias Behrle <mathiasb@m9s.biz>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               jdupes,
               python3-all,
               python3-setuptools,
               python3-sphinx
Standards-Version: 4.7.0
Homepage: https://www.tryton.org/
Vcs-Git: https://salsa.debian.org/tryton-team/tryton-modules-stock-assign-manual.git
Vcs-Browser: https://salsa.debian.org/tryton-team/tryton-modules-stock-assign-manual
Rules-Requires-Root: no

Package: tryton-modules-stock-assign-manual
Architecture: all
Depends: tryton-modules-stock (>= ${version:major}),
         tryton-server (>= ${version:major}),
         ${API},
         ${misc:Depends},
         ${python3:Depends},
         ${sphinxdoc:Depends}
Description: Tryton application platform - stock assign manual module
 Tryton is a high-level general purpose application platform. It is the base
 of a complete business solution as well as a comprehensive health and hospital
 information system (GNUHealth).
 .
 The Stock Assign Manual module adds a wizard on shipments and production that
 allows you to decide from which precise location to pick products.
 .
 Another wizard allows either the whole amount, or a specific quantity, to be
 unassigned of each move.
